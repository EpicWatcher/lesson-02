function returnDemoPromise() {
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            resolve("Done");
        },2000)
    })
}

returnDemoPromise().then(console.log);